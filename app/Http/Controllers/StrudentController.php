<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class StrudentController extends Controller
{
    public function getAll()
    {
        $students = Student::with('college')->get();

        return response()->json([
            'students' => $students,
            'totalCount' => Student::count()
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'firstName' => ['required', 'min:3'],
                'lastName' => ['required'],
                'collegeYear' => ['required'],
                'collegeId' => ['required']
            ]);

        if ($validator->fails())
        {
            return response()->json([
                'errors' => $validator->errors()->toArray()                
            ]);
        }


        $formFields = $request->all();
        Student::create($formFields);

        return response()->json([
            'newStudent' => $request->all()
        ]);
    }

    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();

        return response()->json([
            'error' => false,
            'notice' => 'Success'
        ]);
    }
}
