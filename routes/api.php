<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StrudentController;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::get('/students', [StrudentController::class, 'getAll']);
Route::post('/students', [StrudentController::class, 'store']);
Route::delete('/student/{id}', [StrudentController::class, 'destroy']);